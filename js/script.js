// Найти все параграфы на странице и установить цвет фона #ff0000
const ourParagraphs = document.querySelectorAll("p");
ourParagraphs.forEach(elem => {
    elem.style.backgroundColor = "#ff0000";
})

//Найти элемент с id="optionsList". Вывести в консоль. Найти родительский элемент и вывести в консоль. Найти дочерние ноды, если они есть, и вывести в консоль названия и тип нод.
console.log("---------------------------------------------------");
let ourList = document.getElementById("optionsList");
console.log(ourList);
console.log("---------------------------------------------------");
let parent = document.getElementById("optionsList").parentNode;
console.log(parent);
console.log("---------------------------------------------------");
let node = ourList.childNodes;
console.log(node);

//Установите в качестве контента элемента с классом testParagraph следующий параграф
// This is a paragraph
let isParagraph = document.getElementById("testParagraph");
isParagraph.textContent = "This is a paragraph";

console.log("---------------------------------------------------");
//Получить элементы
// вложенные в элемент с классом main-header и вывеcти их в консоль. Каждому из элементов присвоить новый класс nav-item.
console.log("---------------------------------------------------");

let mainHeader = document.querySelector('.main-header');
let elems = mainHeader.children;

for (let elem of elems) {
    elem.classList.add("nav-item");
}
console.log(mainHeader);
//Найти все элементы с классом section-title. Удалить этот класс у элементов.
document.querySelectorAll('.section-title').forEach((el) => {
    el.classList.remove('section-title')
})